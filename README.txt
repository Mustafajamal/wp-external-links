=== External Links WP  ===
Contributors: webaction
Donate link: http://logicsbuffer.com
Tags: links, external, urls, website links, show links
Requires at least: 4.8
Tested up to: 5.6
Requires PHP: 5.6
Stable tag: 1.0.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This plugin Shows all external links of your site in dashboard. 

== Description ==

**External Links WP ** 
Shows the information about your links in dashboard. 

= Features =

- Information on the number of external links on the page
- Exclude Url's from checking

= Bundled Translations =

- English

== Installation ==

= Automatic installation =

Log in to your WordPress dashboard, navigate to the Plugins menu and click "Add New".

In the search field type "External Links WP " and click Search Plugins. Once you have found the plugin you can view details about it such as the point release, rating and description. You can install it by simply clicking "Install Now".

= Manual installation =

Download the External Links WP  plugin and upload it in your plugins folder.

== Frequently Asked Questions ==

= A question that someone might have =

== Screenshots ==

1. Plugin menu page.
2. Information panel.
3. View external links on the page.

== Changelog ==

= 1.0.0 =

Release Date: January 25, 2019

* Initial release.

== Upgrade Notice ==