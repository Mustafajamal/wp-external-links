<?php

/**
 * Used to mark up the form in the admin panel of the plugin
 *
 * @since      1.0.0
 * @package    Show_External_Links
 * @subpackage Show_External_Links/admin/partials
 */


echo '
<form action="" method="post">
    <h2>' . __( 'External Links WP ', 'show-external-links' ) . '</h2>
        <h4>' . __( 'Enter Urls to exclude', 'show-external-links' ) . '</h4>
            <p>
                <textarea name="sel_skip_sites" rows="6" cols="100" placeholder="' . __( 'Domains or links, all from a new line.', 'show-external-links' ) . PHP_EOL . PHP_EOL . __( 'For example:', 'show-external-links' ) . PHP_EOL . 'example.com' . PHP_EOL . 'example.org' . PHP_EOL . PHP_EOL . __( 'or with parameters:', 'show-external-links' ) . PHP_EOL . 'https://example.com/page/' . PHP_EOL . 'https://example.org/?param=page' . PHP_EOL . PHP_EOL . __( 'URLs and parameters are truncated, only the domain remains.', 'show-external-links' ) . '
                ">' . $option_value . '</textarea>
            </p>
            <p>
                <input type="submit" value="' . __( 'Submit Links', 'show-external-links' ) . '">
            </p>
</form>';

	//$test12 = get_option( 'final_result' );
	//echo $test12;
	
	//Custom
	$args = array(
	  'numberposts' => 100
	);

	$latest_posts = get_posts( $args );
	$post_content = '';
	$page_content = '';
	foreach($latest_posts as $post){
		$post_content .=$post->post_content;
	}
	//print_r($post_content);

	//Get pages
	$pages = get_pages();
	foreach( $pages as $page ) {      
	    $content = $page->post_content;
	    if ( ! $content ) // Check for empty page
	        continue;
	 
	    $page_content .= apply_filters( 'the_content', $content );
    }  

    //Get Header and footer
    // $get_header = get_header();
    // $get_footer = get_footer();
    // $get_sidebar = get_sidebar();
    ob_start();
	get_header();
	$get_header = ob_get_contents();
	ob_end_clean();
	ob_start();
	get_footer();
	$get_footer = ob_get_contents();
	ob_end_clean();
	ob_start();
	get_sidebar();
	$get_sidebar = ob_get_contents();
	ob_end_clean();



	$html = '<p>'.$post_content.''.$page_content.''.$get_header.''.$get_footer.''.$get_sidebar.'</p>';
	//$html = '<p>Welcome to WordPress. This <a href="https://gvalighting.com/">sdfsd</a>is your first post. Edit or delete it, then start writing!</p>';
	$sel_skip_sites = ( is_multisite() ) ? get_site_option( 'sel_skip_sites' ) : get_option( 'sel_skip_sites' );
	//$links_pattern = '/(<a(?![^>]+class=\"ab-item\")[^>]+((https?:\/\/|www)(?!(' . addslashes( $sel_skip_sites) . '))[\w\.\/\-=?#]+)[^>]+>)(.*?)<\/a>/ui';
	$links_pattern = '/(<a(?![^>]+class=\"ab-item\")[^>]+((https?:\/\/|https?:\/\/www)(?!(' . addslashes( $sel_skip_sites) . '))[\w\.\/\-=?#]+)[^>]+>)(.*?)<\/a>/ui';
	//$links_pattern = '';
	$html = str_replace( [ "\t", "\n", "\r" ], '', $html );
	preg_match_all( $links_pattern, $html, $out );

	$list_of_links   = array_map( function( $link ) {
		return wp_parse_url( $link, PHP_URL_HOST );
	}, $out[2] );
	$repetition_rate = array_count_values( $list_of_links );
	$str_links = '';

	foreach ( $repetition_rate as $key => $value ) {
	 	$str_links .= $key . ' (' . $value . ')';
	 }
	
	//Creating Table
	//print_r($str_links);
	?>
	<table class="wp-list-table widefat fixed striped pages">
   <thead>
      <tr>    
         <th scope="col" id="title" class="manage-column column-title column-primary"><a href="#"><span>Url</span><span class="sorting-indicator"></span></a></th>
         <th scope="col" id="author" class="manage-column column-author">Count</th>
      </tr>
   </thead>
   <tbody>
        <?php foreach ( $repetition_rate as $key => $value ){ ?>
		    <tr class="iedit author-self level-0 type-page hentry">
		         <td class="title column-title has-row-actions column-primary page-title" data-colname="Title"><strong><?php echo $key ?></strong></td>
		         <td class="author column-author" data-colname="Author"><?php echo $value ?></td>
		    </tr>
		<?php } ?>
   </tbody>
   <tfoot>
      <tr>      
         <th scope="col" class="manage-column column-primary" style="font-size: 10px;">Externel Links WP By Logics Buffer</th>       
         <th scope="col" class="manage-column column-primary"></th>       
      </tr>
   </tfoot>
</table>
